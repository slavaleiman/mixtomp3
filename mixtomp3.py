#!/usr/bin/python
import sys
import os
files = sys.argv[1:]

cmd = "ffmpeg "
afilter = "-filter_complex \""
fnum = 0
for f in files:
    print(f)
    cmd = cmd + "-i \"" + str(f) + "\" "
    afilter = afilter + "["+ str(fnum) +":a]"
    fnum = fnum + 1

afilter = afilter + "amerge=inputs=" + str(fnum) + '[aout]" -map "[aout]"'

output = " -codec:a mp3 mixdown.mp3"

cmd = cmd + afilter + output 
print(cmd)
os.system(cmd)
